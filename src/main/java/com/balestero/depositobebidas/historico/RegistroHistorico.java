package com.balestero.depositobebidas.historico;

import java.time.LocalDateTime;
import java.util.List;

import com.balestero.depositobebidas.bebida.TipoBebida;
import com.balestero.depositobebidas.secao.Secao;

public class RegistroHistorico {
	
	private String responsavel;
	private HistoricoRepository historicoRepository;
	
	public RegistroHistorico(String responsavel, HistoricoRepository historicoRepository) {
		this.responsavel = responsavel;
		this.historicoRepository = historicoRepository;
	}

	public Historico novo(TipoBebida tipoBebida, double volume, Secao secao) {
		Historico historico = new Historico(LocalDateTime.now(), tipoBebida, volume, secao, responsavel);
		return historicoRepository.save(historico);
	}



}
