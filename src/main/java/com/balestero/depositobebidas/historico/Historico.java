package com.balestero.depositobebidas.historico;

import java.time.LocalDateTime;
import java.util.Objects;

import javax.persistence.*;

import com.balestero.depositobebidas.bebida.TipoBebida;
import com.balestero.depositobebidas.secao.Secao;

@Entity
public class Historico {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Basic
	private LocalDateTime data;

	@Enumerated(EnumType.STRING)
	private TipoBebida tipoBebida;

	private double volume;

	@OneToOne(fetch = FetchType.LAZY,
		cascade = CascadeType.ALL)
	private Secao secao;

	private String responsavel;
	
	public Historico(LocalDateTime data, TipoBebida tipoBebida, double volume, Secao secao, String responsavel) {
		this.data = data;
		this.tipoBebida = tipoBebida;
		this.volume = volume;
		this.secao = secao;
		this.responsavel = responsavel;
	}

	public Long getId() {
		return id;
	}

	public LocalDateTime getData() {
		return data;
	}

	public TipoBebida getTipoBebida() {
		return tipoBebida;
	}

	public double getVolume() {
		return volume;
	}

	public Secao getSecao() {
		return secao;
	}

	public String getResponsavel() {
		return responsavel;
	}

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Historico historico = (Historico) o;
        return Objects.equals(id, historico.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
