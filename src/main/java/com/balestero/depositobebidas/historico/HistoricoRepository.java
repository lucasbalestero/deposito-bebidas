package com.balestero.depositobebidas.historico;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface HistoricoRepository extends JpaRepository<Historico, Long>{

	Historico save(Historico historico);
	List<Historico> findBySecaoId(Long id);
	List<Historico> findAllOrderByData();
    List<Historico> findAllOrderBySecao();

}
