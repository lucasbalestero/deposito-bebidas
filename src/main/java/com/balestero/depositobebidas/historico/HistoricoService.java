package com.balestero.depositobebidas.historico;

import com.balestero.depositobebidas.secao.Secao;

import java.util.List;

public class HistoricoService {


    private HistoricoRepository historicoRepository;

    public HistoricoService(HistoricoRepository historicoRepository) {
        this.historicoRepository = historicoRepository;
    }

    public List<Historico> buscarPorSecao(Secao secao){
        return historicoRepository.findBySecaoId(secao.getId());
    }

    public List<Historico> buscarTodos(){
        return historicoRepository.findAll();
    }
}
