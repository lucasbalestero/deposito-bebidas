package com.balestero.depositobebidas.secao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

import com.balestero.depositobebidas.bebida.Bebida;
import com.balestero.depositobebidas.bebida.TipoBebida;

@Entity
public class Secao {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	
	@Enumerated(EnumType.STRING)
    private TipoBebida tipoBebida;
    
	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<Bebida> bebidas;

    public Secao(TipoBebida tipoBebida) {
        this.tipoBebida = tipoBebida;
    }

    public Secao(){ super();}

    public Long getId() {
        return id;
    }

    public List<Bebida> getBebidas() {
    	if(bebidas == null){ this.bebidas = new ArrayList<>(); }
        return bebidas;
    }

    public void setBebidas(List<Bebida> bebidas) {
        this.bebidas = bebidas;
    }

    public TipoBebida getTipoBebida() {
        return tipoBebida;
    }

    protected void adicionarBebida(Bebida bebida){
        if(bebidas == null){ this.bebidas = new ArrayList<>(); }
        this.bebidas.add(bebida);
    }

	public double getCapacidadeRestante() {
		double total = 0;
		for(Bebida b: this.getBebidas()) {
			total += b.getVolume();
		}
		return tipoBebida.getCapacidade() - total;
	}
}
