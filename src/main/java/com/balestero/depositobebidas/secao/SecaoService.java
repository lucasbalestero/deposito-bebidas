package com.balestero.depositobebidas.secao;

import com.balestero.depositobebidas.bebida.Bebida;
import com.balestero.depositobebidas.bebida.BebidaRepository;
import com.balestero.depositobebidas.excecoes.TipoBebidaNaoPermitidoException;
import com.balestero.depositobebidas.historico.HistoricoRepository;
import com.balestero.depositobebidas.historico.RegistroHistorico;

import java.util.List;

public class SecaoService {

    private GerenciadorDeSecao gerenciadorDeSecao;
    private String responsavel;

    public SecaoService(SecaoRepository secaoRepository, HistoricoRepository historicoRepository,
                        BebidaRepository bebidaRepository, String responsavel) {
        gerenciadorDeSecao = new GerenciadorDeSecao(secaoRepository, historicoRepository, bebidaRepository);
        this.responsavel = responsavel;
    }

    public Secao salvar(Secao secao) {
        return gerenciadorDeSecao.salvar(secao, responsavel);
    }

    public Secao adicionarBebida(Secao secao, Bebida bebida) throws TipoBebidaNaoPermitidoException {
        return gerenciadorDeSecao.adicionarBebida(secao, bebida, responsavel);
    }

    public List<Secao> buscarTodas(){
        return gerenciadorDeSecao.buscarTodas();
    }
}
