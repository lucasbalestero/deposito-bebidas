package com.balestero.depositobebidas.secao;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface SecaoRepository extends JpaRepository<Secao, Long> {

    Secao save(Secao secao);

    List<Secao> findAll();
}
