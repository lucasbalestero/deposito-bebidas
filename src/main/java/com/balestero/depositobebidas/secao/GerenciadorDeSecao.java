package com.balestero.depositobebidas.secao;

import com.balestero.depositobebidas.bebida.Bebida;
import com.balestero.depositobebidas.bebida.BebidaRepository;
import com.balestero.depositobebidas.excecoes.TipoBebidaNaoPermitidoException;
import com.balestero.depositobebidas.historico.HistoricoRepository;
import com.balestero.depositobebidas.historico.RegistroHistorico;

import java.util.List;

public class GerenciadorDeSecao {

    private SecaoRepository ser;
    private HistoricoRepository hir;
    private BebidaRepository ber;

    public GerenciadorDeSecao(SecaoRepository ser, HistoricoRepository hir, BebidaRepository ber) {
        this.ser = ser;
        this.hir = hir;
        this.ber = ber;
    }

    public Secao salvar(Secao secao, String responsavel){
        secao = ser.save(secao);
        try {
            secao = this.adicionarBebidas(secao, secao.getBebidas(), responsavel);
        } catch (TipoBebidaNaoPermitidoException e) {
            e.printStackTrace();
        }
        return secao;
    }
    private Secao adicionarBebidas(Secao secao, List<Bebida> bebidas, String responsavel) throws TipoBebidaNaoPermitidoException {
        for(Bebida b: bebidas){
                secao = adicionarBebida(secao, b, responsavel);
        }
        return secao;
    }

    public Secao adicionarBebida(Secao secao, Bebida bebida, String responsavel) throws TipoBebidaNaoPermitidoException {
        if(!secao.getTipoBebida().equals(bebida.getTipoBebida())){
            throw new TipoBebidaNaoPermitidoException();
        }
        RegistroHistorico registroHistorico = new RegistroHistorico(responsavel, hir);

        ber.save(bebida);

        secao.adicionarBebida(bebida);
        secao = ser.save(secao);

        registroHistorico.novo(bebida.getTipoBebida(), bebida.getVolume(), secao);

        return secao;
    }

    public List<Secao> buscarTodas(){
        return ser.findAll();
    }
}
