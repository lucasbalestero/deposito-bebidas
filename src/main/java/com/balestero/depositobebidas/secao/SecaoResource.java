package com.balestero.depositobebidas.secao;

import com.balestero.depositobebidas.bebida.BebidaRepository;
import com.balestero.depositobebidas.historico.HistoricoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/secoes")
public class SecaoResource {


    @Autowired
    private SecaoRepository secaoRepository;

    @Autowired
    private HistoricoRepository historicoRepository;

    @Autowired
    private BebidaRepository bebidaRepository;

    private SecaoService secaoService;

    @PostMapping
    public Secao salvar(@RequestBody Secao secao){
        secaoService = new SecaoService(secaoRepository, historicoRepository, bebidaRepository, "Lucas");
        return secaoService.salvar(secao);
    }

    @GetMapping("/todas")
    public ResponseEntity<List<Secao>> buscarTodas(){
        secaoService = new SecaoService(secaoRepository, historicoRepository, bebidaRepository, "Lucas");
        return new ResponseEntity<>(secaoService.buscarTodas(), HttpStatus.OK);
    }


}
