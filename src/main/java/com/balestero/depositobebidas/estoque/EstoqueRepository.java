package com.balestero.depositobebidas.estoque;

public interface EstoqueRepository{
    Estoque save(Estoque estoque);
    
    Estoque findById(Long id);
}
