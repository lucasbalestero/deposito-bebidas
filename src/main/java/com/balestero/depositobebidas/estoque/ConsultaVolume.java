package com.balestero.depositobebidas.estoque;

import java.util.List;
import java.util.stream.Collectors;

import com.balestero.depositobebidas.bebida.TipoBebida;
import com.balestero.depositobebidas.secao.Secao;

public class ConsultaVolume {

	private Estoque estoque;
	
	public ConsultaVolume(Estoque estoque) {
		this.estoque = estoque;
	}

	public double buscaVolumeTotal(TipoBebida tipoBebida) {
		double total = 0;
		
		for(Secao s: estoque.getSecoes()) {
			if(s.getTipoBebida().equals(tipoBebida)) {
				total += s.getTipoBebida().getCapacidade();
			}
		};
		return total;
	}

	public List<Secao> buscaSecoesDisponiveisPorTipo(TipoBebida tipoBebida) {
		List<Secao> secoes = estoque.getSecoes().stream()
				.filter(s -> s.getTipoBebida() == tipoBebida && s.getCapacidadeRestante() > 0 )
				.collect(Collectors.toList());
		return secoes;
	}

	public List<Secao> buscaSecoesComBebidas(TipoBebida tipoBebida) {
		List<Secao> secoes = estoque.getSecoes().stream()
				.filter(s -> s.getTipoBebida() == tipoBebida && s.getBebidas().size() > 0)
				.collect(Collectors.toList());
		return secoes;
	}
	
	

}
