package com.balestero.depositobebidas.estoque;

import javax.persistence.*;

import com.balestero.depositobebidas.secao.Secao;

import java.util.ArrayList;
import java.util.List;

@Entity
public class Estoque {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long id;


    @OneToMany(fetch = FetchType.LAZY)
    public List<Secao> secoes;

    public Estoque(Secao... secoes){

        for (Secao secao: secoes) {
            adicionarSecao(secao);
        }
    }

    protected void adicionarSecao(Secao secao){
        if(this.secoes == null) { this.secoes = new ArrayList<Secao>(); }

        this.secoes.add(secao);
    }

    public List<Secao> getSecoes(){
        return this.secoes;
    }

    public int buscaQuantidadeSecoes(){
        return secoes.size();
    }
}
