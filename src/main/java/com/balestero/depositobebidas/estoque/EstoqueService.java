package com.balestero.depositobebidas.estoque;

import com.balestero.depositobebidas.excecoes.LimiteDeSecoesException;
import com.balestero.depositobebidas.secao.Secao;

public class EstoqueService {

    private EstoqueRepository estoqueRepository;

    public EstoqueService(EstoqueRepository estoqueRepository) {
        this.estoqueRepository = estoqueRepository;
    }

    public Estoque salvar(Estoque estoque) {
        return estoqueRepository.save(estoque);
    }


    public Estoque adicionarSecao(Estoque estoque, Secao secao) throws LimiteDeSecoesException {
        if(estoque.buscaQuantidadeSecoes() > 5){
            throw new LimiteDeSecoesException();
        }
        estoque.adicionarSecao(secao);
        
        return this.salvar(estoque);
    }

	public Estoque buscarPeloId(Long id) {
		return estoqueRepository.findById(id);
	}
}
