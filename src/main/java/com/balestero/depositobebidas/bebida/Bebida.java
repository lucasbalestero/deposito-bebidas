package com.balestero.depositobebidas.bebida;

import java.util.Objects;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Bebida {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
    private String nome;
    
    @Enumerated(EnumType.STRING)
    private TipoBebida tipoBebida;
    
    private double volume;

    public Bebida(String nome, double volume, TipoBebida tipoBebida) {
        this.nome = nome;
        this.volume = volume;
        this.tipoBebida = tipoBebida;
    }

    public Bebida(){}


    public String getNome() {
        return nome;
    }

    public Long getId() { return this.id; }
    
    public double getVolume() { return this.volume; }

    public TipoBebida getTipoBebida() {
        return tipoBebida;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Bebida bebida = (Bebida) o;
        return Objects.equals(nome, bebida.nome) &&
                tipoBebida == bebida.tipoBebida;
    }

    @Override
    public int hashCode() {
        return Objects.hash(nome, tipoBebida);
    }
}
