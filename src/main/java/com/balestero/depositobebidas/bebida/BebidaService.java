package com.balestero.depositobebidas.bebida;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class BebidaService {

    @Autowired
    private BebidaRepository bebidaRepository;

    public BebidaService(BebidaRepository bebidaRepository) {
        this.bebidaRepository = bebidaRepository;
    }

    public BebidaService(){

    }

    public Bebida salvar(Bebida cerveja) {
        return bebidaRepository.save(cerveja);
    }

    public Bebida buscarPeloId(Long id) {
        return bebidaRepository.findById(id).get();
    }
}
