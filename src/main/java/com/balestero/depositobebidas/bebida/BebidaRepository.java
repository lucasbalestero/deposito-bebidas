package com.balestero.depositobebidas.bebida;

import org.springframework.data.jpa.repository.JpaRepository;

public interface BebidaRepository extends JpaRepository<Bebida, Long> {
    Bebida save(Bebida cerveja);

}
