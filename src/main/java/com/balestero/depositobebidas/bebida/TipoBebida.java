package com.balestero.depositobebidas.bebida;

public enum TipoBebida {
    Alcoolica(500),
    NaoAlcoolica(400);
    
	private double capacidade;
	
	TipoBebida(double capacidade){
		this.capacidade = capacidade;
	}
	
    public double getCapacidade() {
    	return this.capacidade;
    }
}
