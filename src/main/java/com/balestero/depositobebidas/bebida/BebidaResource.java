package com.balestero.depositobebidas.bebida;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/bebidas")
public class BebidaResource {


    @Autowired
    private BebidaRepository bebidaRepository;


    @GetMapping("/{id}")
    public ResponseEntity<Bebida> buscarPeloId(@PathVariable("id") Long id){

        BebidaService bebidaService = new BebidaService(bebidaRepository);

        Bebida bebida = bebidaService.buscarPeloId(id);

        return new ResponseEntity<>(bebida, HttpStatus.OK);
    }

    @GetMapping("/nova/{nome}")
    public ResponseEntity<Bebida> salvar(@PathVariable("nome") String nome){
        Bebida bebida = new Bebida(nome, 0.6, TipoBebida.Alcoolica);

        BebidaService bebidaService = new BebidaService(bebidaRepository);

        Bebida bebidaSalva = bebidaService.salvar(bebida);

        return new ResponseEntity<>(bebidaSalva, HttpStatus.OK);
    }

}
