package com.balestero.depositobebidas.test;

import static org.junit.Assert.assertEquals;

import java.util.List;

import com.balestero.depositobebidas.bebida.BebidaRepository;
import com.balestero.depositobebidas.historico.HistoricoRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import com.balestero.depositobebidas.bebida.Bebida;
import com.balestero.depositobebidas.bebida.TipoBebida;
import com.balestero.depositobebidas.estoque.ConsultaVolume;
import com.balestero.depositobebidas.estoque.Estoque;
import com.balestero.depositobebidas.estoque.EstoqueRepository;
import com.balestero.depositobebidas.estoque.EstoqueService;
import com.balestero.depositobebidas.mock.MockEstoque;
import com.balestero.depositobebidas.secao.Secao;
import com.balestero.depositobebidas.secao.SecaoRepository;
import com.balestero.depositobebidas.secao.SecaoService;


@RunWith(SpringRunner.class)
public class ConsultasVolumeTest {

	@MockBean
	private EstoqueRepository estoqueRepository;
	private EstoqueService estoqueService;
	
	@MockBean
	private SecaoRepository secaoRepository;
	private SecaoService secaoService;

	@MockBean
	private HistoricoRepository historicoRepository;

	@MockBean
	private BebidaRepository bebidaRepository;
	
	private ConsultaVolume consultaVolume;
	
	private Estoque estoque;
	
	@Before
	public void set_up() throws Exception {
		estoqueService = new EstoqueService(estoqueRepository);
		secaoService = new SecaoService(secaoRepository, historicoRepository, bebidaRepository,  "Lucas");
		
		Secao s1 = MockEstoque.s1;
		Secao s2 = MockEstoque.s2;
		Secao s3 = MockEstoque.s3;
		Secao s4 = MockEstoque.s4;
		Secao s5 = MockEstoque.s5;

		String responsavel = "Lucas";
		
		secaoService.adicionarBebida(s3, new Bebida("Refrigerante", 2, TipoBebida.NaoAlcoolica));
		secaoService.adicionarBebida(s2, new Bebida("Mega Barril de Chopp", 500, TipoBebida.Alcoolica));
		
		estoque = new Estoque(s1, s2, s3, s4, s5); 
		
		consultaVolume = new ConsultaVolume(estoque);
	}

	
	@Test
	public void deve_trazer_volume_total_por_tipo_bebida() {
		
		assertEquals(3 * 500, consultaVolume.buscaVolumeTotal(TipoBebida.Alcoolica), 0.01);
	}
	
	
	@Test
	public void deve_buscar_secoes_disponiveis_para_armazenamento_por_tipo_bebida() {
		
		List<Secao> secoes = consultaVolume.buscaSecoesDisponiveisPorTipo(TipoBebida.Alcoolica);
		
		assertEquals(2, secoes.size());
		
	}
	
	@Test
	public void deve_buscar_secoes_com_bebida_armazenada_por_tipo_bebida() {
		
		List<Secao> secoes = consultaVolume.buscaSecoesComBebidas(TipoBebida.NaoAlcoolica);
		
		assertEquals(1, secoes.size());
		
	}
	
}
