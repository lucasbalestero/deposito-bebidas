package com.balestero.depositobebidas.test;

import com.balestero.depositobebidas.bebida.Bebida;
import com.balestero.depositobebidas.bebida.BebidaRepository;
import com.balestero.depositobebidas.bebida.BebidaService;
import com.balestero.depositobebidas.bebida.TipoBebida;
import com.balestero.depositobebidas.excecoes.TipoBebidaNaoPermitidoException;
import com.balestero.depositobebidas.historico.HistoricoRepository;
import com.balestero.depositobebidas.secao.Secao;
import com.balestero.depositobebidas.secao.SecaoRepository;
import com.balestero.depositobebidas.secao.SecaoService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
public class CadastroDeBebidaTest {

    @MockBean
    private SecaoRepository secaoRepository;

    @MockBean
    private BebidaRepository bebidaRepository;

    @MockBean
    private HistoricoRepository historicoRepository;

    private BebidaService bebidaService;
    private SecaoService secaoService;

    private Secao secao;
    private Bebida cerveja;
    private Bebida suco;

    @Before
    public void set_up() throws Exception {

        secaoService = new SecaoService(secaoRepository, historicoRepository, bebidaRepository, "Lucas");
        bebidaService = new BebidaService(bebidaRepository);

        cerveja = new Bebida("Cerveja", 0.6, TipoBebida.Alcoolica);
        suco = new Bebida("Suco de Laranja", 1, TipoBebida.NaoAlcoolica);
        secao = new Secao(TipoBebida.Alcoolica);

    }


    @Test
    public void deve_cadastrar_bebida(){
        bebidaService.salvar(cerveja);

        verify(bebidaRepository).save(cerveja);
    }


    @Test
    public void deve_adicionar_bebida_na_secao() throws Exception {
        secaoService.adicionarBebida(secao, cerveja);

        verify(secaoRepository).save(secao);
    }


    @Test(expected = TipoBebidaNaoPermitidoException.class)
    public void deve_dar_excessao_ao_adicionar_bebida_com_tipo_invalido_na_secao() throws Exception {
        when(secaoService.adicionarBebida(secao, suco)).thenReturn(secao);
    }

}
