package com.balestero.depositobebidas.test;

import com.balestero.depositobebidas.bebida.BebidaRepository;
import com.balestero.depositobebidas.bebida.TipoBebida;
import com.balestero.depositobebidas.historico.HistoricoRepository;
import com.balestero.depositobebidas.secao.Secao;
import com.balestero.depositobebidas.secao.SecaoRepository;
import com.balestero.depositobebidas.secao.SecaoService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import static org.mockito.Mockito.verify;

@RunWith(SpringRunner.class)
public class CadastroDeSecaoTest {

    @MockBean
    private SecaoRepository secaoRepository;

    @MockBean
    private HistoricoRepository historicoRepository;

    @MockBean
    private BebidaRepository bebidaRepository;

    private SecaoService secaoService;

    @Before
    public void set_up() throws Exception {

        secaoService = new SecaoService(secaoRepository, historicoRepository, bebidaRepository, "Lucas");

    }


    @Test
    public void deve_cadastrar_secao_com_tipo_bebida(){
        Secao secao = new Secao(TipoBebida.Alcoolica);

        secaoService.salvar(secao);

        verify(secaoRepository).save(secao);
    }
}
