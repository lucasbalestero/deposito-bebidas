package com.balestero.depositobebidas.test;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import com.balestero.depositobebidas.bebida.TipoBebida;
import com.balestero.depositobebidas.estoque.Estoque;
import com.balestero.depositobebidas.estoque.EstoqueRepository;
import com.balestero.depositobebidas.estoque.EstoqueService;
import com.balestero.depositobebidas.excecoes.LimiteDeSecoesException;
import com.balestero.depositobebidas.secao.Secao;



@RunWith(SpringRunner.class)

public class CadastroDeEstoqueTest {


    @MockBean
    private EstoqueRepository estoqueRepository;

    private EstoqueService estoqueService;

    private Secao s1;
    private Secao s2;
    private Secao s3;
    private Secao s4;
    private Secao s5;

    @Before
    public void set_up() throws Exception {

        estoqueService = new EstoqueService(estoqueRepository);

        s1 = new Secao(TipoBebida.Alcoolica);
        s2 = new Secao(TipoBebida.Alcoolica);
        s3 = new Secao(TipoBebida.NaoAlcoolica);
        s4 = new Secao(TipoBebida.NaoAlcoolica);
        s5 = new Secao(TipoBebida.Alcoolica);

    }


    @Test
    public void deve_inserir_ate_5_secoes(){
        Estoque estoque = new Estoque(s1, s2, s3, s4, s5);

        estoqueService.salvar(estoque);

        verify(estoqueRepository).save(estoque);
    }

    @Test(expected = LimiteDeSecoesException.class)
    public void deve_dar_excessao_ao_adicionar_mais_que_5_secoes() throws LimiteDeSecoesException {
        Secao s6 = new Secao(TipoBebida.NaoAlcoolica);

        Estoque estoque = new Estoque(s1, s2, s3, s4, s5, s6);

        when(estoqueService.adicionarSecao(estoque, s6)).thenReturn(estoque);
    }
    
}