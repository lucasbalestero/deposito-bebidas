package com.balestero.depositobebidas.test;

import static org.mockito.Mockito.verify;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import com.balestero.depositobebidas.bebida.Bebida;
import com.balestero.depositobebidas.bebida.TipoBebida;
import com.balestero.depositobebidas.historico.Historico;
import com.balestero.depositobebidas.historico.HistoricoRepository;
import com.balestero.depositobebidas.historico.RegistroHistorico;
import com.balestero.depositobebidas.secao.Secao;

import java.time.LocalDateTime;

@RunWith(SpringRunner.class)
public class CadastroHistoricoTest {

	@MockBean
	private HistoricoRepository historicoRepository;
	
	private RegistroHistorico registro;

	private String responsavel;
	
	@Before
	public void set_up() {
		responsavel = "Lucas";
		registro = new RegistroHistorico(responsavel, historicoRepository);
	}
	
	@Test
	public void deve_registrar_historico_ao_adicionar_bebida_na_secao() {
		Bebida bebida = new Bebida("Cerveja", 0.6, TipoBebida.Alcoolica);
		Secao secao = new Secao(TipoBebida.Alcoolica);
		
		
		Historico historico = new Historico(LocalDateTime.now(), bebida.getTipoBebida(), bebida.getVolume(),
				secao, responsavel);
		registro.novo(bebida.getTipoBebida(), bebida.getVolume(), secao);
		
		verify(historicoRepository).save(historico);
		
	}
}
