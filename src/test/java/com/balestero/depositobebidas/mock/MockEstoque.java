package com.balestero.depositobebidas.mock;

import com.balestero.depositobebidas.bebida.TipoBebida;
import com.balestero.depositobebidas.secao.Secao;

public class MockEstoque {

	public static Secao s1 = new Secao(TipoBebida.Alcoolica);
	public static Secao s2 = new Secao(TipoBebida.Alcoolica);
	public static Secao s3 = new Secao(TipoBebida.NaoAlcoolica);
	public static Secao s4 = new Secao(TipoBebida.NaoAlcoolica);
	public static Secao s5 = new Secao(TipoBebida.Alcoolica);
	
}
