# Depósito de Bebidas

Aplicação que tem como objetivo controlar o fluxo num depósito de bebidas.

Através da API é possível:
* Realizar o **cadastro** e **consulta** das **bebidas em cada seção**
* Consultar o **volume total no estoque**, **por tipo de bebida**. Ex:
###### Seção 1
| Tipo         | Volume Total|
|--------------|:------------|
| Alcoólica    | 1500L       |
| Não Alcoólica| 800L        |

* Consulta das seções com **volume disponível** pelo tipo de bebida
* Consulta das seções que possuem determinado **tipo de bebida em estoque**
* Cadastro de **histórico** de entrada e saída de bebida nas seções
* Consulta de histórico por tipo de bebida e seção


Regras para o fluxo de cadastro e cálculo:
* Uma seção não pode ter dois ou mais tipos diferentes de bebidas (como já fora dito).
* Não há entrada ou saída de estoque sem respectivo registro no histórico.
* Registro deve conter horário, tipo, volume, seção e responsável pela entrada.
* O endpoint de consulta de histórico de entrada e saída de estoque, deve retornar os
resultados ordenados por data e seção, podendo alterar a ordenação via parâmetros.
* Para situações de erro, é necessário que a resposta da requisição seja coerente em
exibir uma mensagem condizente com o erro.

### Ferramentas
O projeto utiliza Java e Spring Boot, bem como suas bibliotecas de testes (JUnit, Mockito).


